package view;


import java.sql.SQLException;
import java.util.ArrayList;

import controller.Controlador;
import model.vo.Lider;
import model.vo.Proyecto;

public class Vista {

    public static final Controlador controlador = new Controlador();

    public static void vista_requerimiento_1() {

        try {

            ArrayList<Proyecto> proyectos = controlador.Solucionar_requerimiento_1();
            for (int i = 0; i < proyectos.size(); i++) {
                Proyecto proyecto = proyectos.get(i);

                System.out.println("Fecha_Inicio: " + proyecto.getFecha_inicio() + " - Numero_Habitaciones: " + proyecto.getNum_habitaciones() + " - Numero_Banos: " + proyecto.getNum_banios());
            }

        } catch (SQLException e) {
            System.err.println("Ha ocurrido un error!" + e.getMessage());
        }

    }

    public static void vista_requerimiento_2() {
        try {

            ArrayList<Proyecto> proyectos = controlador.Solucionar_requerimiento_2();
            for (int i = 0; i < proyectos.size(); i++) {
                Proyecto proyecto = proyectos.get(i);

                System.out.println("Fecha_Inicio: " + proyecto.getFecha_inicio() + " - Numero_Habitaciones: " + proyecto.getNum_habitaciones() + " - Numero_Banos: " + proyecto.getNum_banios() + " - Nombre_Lider: " + proyecto.getLider().getNombre() +" - Apellido_Lider: "+ proyecto.getLider().getApellido()+ " - Estrado_Proyecto: " + proyecto.getEstrato_proyecto());
            }

        } catch (SQLException e) {
            System.err.println("Ha ocurrido un error!" + e.getMessage());
        }
    }

    public static void vista_requerimiento_3() {
        try {

            ArrayList<Proyecto> proyectos = controlador.Solucionar_requerimiento_3();
            for (int i = 0; i < proyectos.size(); i++) {
                Proyecto proyecto = proyectos.get(i);

                System.out.println("Total_Habitaciones: " + proyecto.getNum_habitaciones() + " - Constructora: " + proyecto.getNombre_constructora());
            }

        } catch (SQLException e) {
            System.err.println("Ha ocurrido un error!" + e.getMessage());
        }
    }

    public static void vista_requerimiento_4() {
        try {

            ArrayList<Lider> lideres = controlador.Solucionar_requerimiento_4();
            for (int j = 0; j < lideres.size(); j++) {
                Lider lider = lideres.get(j);

            System.out.println("Nombre_Lider: " + lider.getNombre() + " - Apellido_Lider: " + lider.getApellido());
            }

        } catch (SQLException e) {
            System.err.println("Ha ocurrido un error!" + e.getMessage());
        }
    }

    public static void vista_requerimiento_5() {
        try {

            ArrayList<Proyecto> proyectos = controlador.Solucionar_requerimiento_5();
            for (int i = 0; i < proyectos.size(); i++) {
                Proyecto proyecto = proyectos.get(i);

                System.out.println("Total_Habitaciones: " + proyecto.getNum_habitaciones() + " - Constructora: " + proyecto.getNombre_constructora());
            }

        } catch (SQLException e) {
            System.err.println("Ha ocurrido un error!" + e.getMessage());
        }
    }

}
