package model.dao;

//Estructura de datos
import java.util.ArrayList;

//Librerías para SQL y Base de Datos
import java.sql.SQLException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

//Clase para conexión
import util.JDBCUtilities;

//Encapsulamiento de los datos
import model.vo.Lider;

public class LiderDao {

    public ArrayList<Lider> query_requerimiento_4() throws SQLException {
        //Connection conexion = JDBCUtilities.getConnection();
        // Crea arreglo para almacenar objetos tipo Proyecto
        ArrayList<Lider> lideres = new ArrayList<Lider>();

        try {
            Statement stmt = JDBCUtilities.getConnection().createStatement();
            ResultSet rs = stmt.executeQuery("SELECT Nombre, Primer_Apellido FROM Lider INNER JOIN Proyecto ON Lider.ID_Lider=Proyecto.ID_Lider WHERE Constructora=\"Pegaso\"");
            
            while (rs.next()) {
                Lider lider = new Lider();

                lider.setNombre(rs.getString("Nombre"));
                lider.setApellido(rs.getString("Primer_Apellido"));

                lideres.add(lider);
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        
        return lideres;
    }// Fin del método query_requerimiento_4

}