package model.dao;

//Estructura de datos
import java.util.ArrayList;

import model.vo.Lider;
import model.vo.Proyecto;

//Librerías para SQL y Base de Datos
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

//Clase para conexión
import util.JDBCUtilities;

public class ProyectoDao {

    public ProyectoDao() {

    }

    public ArrayList<Proyecto> query_requerimiento_1() throws SQLException {

        ArrayList<Proyecto> proyectos = new ArrayList<Proyecto>();

        try {
            Statement stmt = JDBCUtilities.getConnection().createStatement();
            ResultSet rs = stmt.executeQuery("SELECT Fecha_Inicio, Numero_Banos, Numero_Habitaciones FROM Proyecto WHERE Constructora=\"Pegaso\"");

            while (rs.next()) {
                Proyecto proyecto = new Proyecto();

                proyecto.setFecha_inicio(rs.getString("Fecha_Inicio"));
                proyecto.setNum_banios(rs.getInt("Numero_Banos"));
                proyecto.setNum_habitaciones(rs.getInt("Numero_Habitaciones"));

                proyectos.add(proyecto);
            }
        } catch (Exception e) {
            System.out.println(e);
        }

        return proyectos;
    }

    public ArrayList<Proyecto> query_requerimiento_2() throws SQLException {
        ArrayList<Proyecto> proyectos = new ArrayList<Proyecto>();

        try {
            Statement stmt = JDBCUtilities.getConnection().createStatement();
            ResultSet rs = stmt.executeQuery("SELECT p.Fecha_Inicio, p.Numero_Habitaciones, p.Numero_Banos, l.Nombre, l.Primer_Apellido, t.Estrato " +
                "FROM Proyecto as p " +
                "INNER JOIN Lider as l ON p.ID_Lider=l.ID_Lider " +
                "INNER JOIN Tipo as t ON p.ID_Tipo=t.ID_Tipo " +
                "WHERE p.Constructora=\"Pegaso\" LIMIT 50");

            while (rs.next()) {
                Proyecto proyecto = new Proyecto();
                Lider lider = new Lider();

                lider.setNombre(rs.getString("Nombre"));
                lider.setApellido(rs.getString("Primer_Apellido"));

                proyecto.setLider(lider);
                proyecto.setFecha_inicio(rs.getString("Fecha_Inicio"));
                proyecto.setNum_banios(rs.getInt("Numero_Banos"));
                proyecto.setNum_habitaciones(rs.getInt("Numero_Habitaciones"));
                proyecto.setEstrato_proyecto(rs.getInt("Estrato"));

                proyectos.add(proyecto);
            }
        } catch (Exception e) {
            System.out.println(e);
        }

        return proyectos;
    }// Fin del método query_requerimiento_2


    public ArrayList<Proyecto> query_requerimiento_3() throws SQLException {
        
        ArrayList<Proyecto> proyectos = new ArrayList<Proyecto>();

        try {
            Statement stmt = JDBCUtilities.getConnection().createStatement();
            ResultSet rs = stmt.executeQuery("SELECT sum(Numero_Habitaciones) AS Total_Habitaciones, Constructora FROM Proyecto GROUP by Constructora");
            
            while (rs.next()) {
                Proyecto proyecto = new Proyecto();

                proyecto.setNum_habitaciones (rs.getInt("Total_Habitaciones"));
                proyecto.setNombre_constructora(rs.getString("Constructora"));

                proyectos.add(proyecto);
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return proyectos;
        
    }// Fin del método query_requerimiento_3


    public ArrayList<Proyecto> query_requerimiento_5() throws SQLException{
        ArrayList<Proyecto> proyectos = new ArrayList<Proyecto>();

        try {
            Statement stmt = JDBCUtilities.getConnection().createStatement();
            ResultSet rs = stmt.executeQuery("SELECT sum(Numero_Habitaciones) AS Total_Habitaciones, Constructora FROM Proyecto GROUP BY Constructora HAVING Total_Habitaciones>200 ORDER BY Total_Habitaciones ASC");
            
            while (rs.next()) {
                Proyecto proyecto = new Proyecto();

                proyecto.setNum_habitaciones (rs.getInt("Total_Habitaciones"));
                proyecto.setNombre_constructora(rs.getString("Constructora"));

                proyectos.add(proyecto);
            }
        } catch (Exception e) {
            System.out.println(e);
        }        
        return proyectos;
        
    }// Fin del método query_requerimiento_4
    
}